package com.patendencia.main;


/**
 * Implementação original disponível no link 
 * https://pt.stackoverflow.com/questions/4367/como-implementar-um-algoritmo-de-regress%C3%A3o-linear
 *
 */
public class CalculoTendencia {
	
	/**
	 * 
	 * @param sequenciaY, vetor com quantidades de venda de um produto A
	 * @param eixoX, vetor com meses de venda de um produto A
	 * @param novoValorX, valor indicando a posição do novo valor a ser descoberto da sequenciaY
	 * 
	 * @return valor de tendencia para a sequenciaX
	 */
	public double obterValorFuturo(double[] sequenciaY, double[] eixoX, double novoValorX) {
		double[] valores = leastSquaresFitLinear(sequenciaY, eixoX);
		return (valores[0] * novoValorX) + valores[1];
	}

	private double[] leastSquaresFitLinear(double[] sequenciaY, double[] eixoX) {
		double m, b;
		
		if (sequenciaY.length != eixoX.length) {
			return new double[] { 0, 0 };
		}

		int numPontos = sequenciaY.length;

		double x1, y1, xy, x2, j;

		x1 = y1 = xy = x2 = 0.0;
		for (int i = 0; i < numPontos; i++) {
			x1 = x1 + eixoX[i];
			y1 = y1 + sequenciaY[i];
			xy = xy + eixoX[i] * sequenciaY[i];
			x2 = x2 + eixoX[i] * eixoX[i];
		}

		m = b = 0;
		j = ((double) numPontos * x2) - (x1 * x1);

		if (j != 0.0) {
			m = (((double) numPontos * xy) - (x1 * y1)) / j;
			b = ((y1 * x2) - (x1 * xy)) / j;
		}
		return new double[] { m, b };
	}

}
