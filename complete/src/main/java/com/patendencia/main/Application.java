package com.patendencia.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration
@EntityScan("com.patendencia.domain")
@ComponentScan("com.patendencia.controller")
@EnableJpaRepositories("com.patendencia.repository")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    @SuppressWarnings("unused")
	private static void teste(){
		CalculoTendencia calculoTendencia = new CalculoTendencia();
		
		double[] mesesVendaProduto = { 1, 2, 3, 4, 5 };
		double[] vendasProduto = { 10, 20, 30, 40, 50 };
		
		System.out.println(calculoTendencia.obterValorFuturo(vendasProduto, mesesVendaProduto, 6));
	}
}
