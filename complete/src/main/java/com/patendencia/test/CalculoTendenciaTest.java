package com.patendencia.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

import com.patendencia.main.CalculoTendencia;

public class CalculoTendenciaTest {
	
	private CalculoTendencia calculoTendencia;
	
	@Before
	public void setUp() {
		calculoTendencia = new CalculoTendencia();
	}

	@Test
	public void deveCalcularTendenciaCorreta() {
		assertEquals(60, calculoTendencia.obterValorFuturo(obterVendasProduto(), obterMesesVendaProduto(), 6), 0);
	}
	
	@Test
	public void deveCalcularTendenciaIncorretaQuandoValorFuturoIncorreto() {
		assertNotEquals(60, calculoTendencia.obterValorFuturo(obterVendasProduto(), obterMesesVendaProduto(), 7), 0);
	}
	
	@Test
	public void deveCalcularTendenciaIncorretaQuandoSequenciasDeVendasInvertidas() {
		assertNotEquals(60, calculoTendencia.obterValorFuturo(obterMesesVendaProduto(), obterVendasProduto(), 6), 0);
	}
	
	@Test
	public void deveCalcularTendenciaIncorretaQuandoSequenciasDeVendasTamanhosDiferentes() {
		double[] vendasProdutoAModificado = {1, 2, 3, 4, 5, 6};
		assertNotEquals(60, calculoTendencia.obterValorFuturo(obterVendasProduto(), vendasProdutoAModificado, 6), 0);
	}
	
	private double[] obterMesesVendaProduto() {
		return new double[] {1, 2, 3, 4, 5};
	}
	
	private double[] obterVendasProduto() {
		return new double[] {10, 20, 30, 40, 50};
	}

}
