package com.patendencia.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.patendencia.domain.Produto;
import com.patendencia.repository.ProdutoRepository;

@Controller
@RequestMapping(path="/produto")
public class ProdutoController {
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@GetMapping(path="/add")
	@ResponseBody
	public String addNewUser (@RequestParam String nome) {
		Produto produto = new Produto();
		produto.setNome(nome);
		produtoRepository.save(produto);
		
		return "Produto salvo com sucesso";
	}
	
	@GetMapping(path="/all")
	@ResponseBody
	public Iterable<Produto> getAllUsers() {
		
		return produtoRepository.findAll();
	}
}
