package com.patendencia.repository;

import org.springframework.data.repository.CrudRepository;

import com.patendencia.domain.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Long> {

}
