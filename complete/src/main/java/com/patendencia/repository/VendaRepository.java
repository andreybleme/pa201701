package com.patendencia.repository;

import org.springframework.data.repository.CrudRepository;

import com.patendencia.domain.Venda;

public interface VendaRepository extends CrudRepository<Venda, Long> {

}
