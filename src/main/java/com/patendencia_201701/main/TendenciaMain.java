package com.patendencia_201701.main;

public class TendenciaMain {
	public static void main(String[] args) {
		
		CalculoTendencia calculoTendencia = new CalculoTendencia();
		
		double[] mesesVendaProduto = { 1, 2, 3, 4, 5 };
		double[] vendasProduto = { 10, 20, 30, 40, 50 };
		
		System.out.println(calculoTendencia.obterValorFuturo(vendasProduto, mesesVendaProduto, 6));
	}
}